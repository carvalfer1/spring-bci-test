package com.evaluation.users.exceptions;

public class GlobalExceptionHandler extends RuntimeException {
    public GlobalExceptionHandler(String message)
    {
        super(message);
    }
}
