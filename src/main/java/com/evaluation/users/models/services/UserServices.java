package com.evaluation.users.models.services;

import com.evaluation.users.exceptions.GlobalExceptionHandler;
import com.evaluation.users.models.dao.IUserDao;
import com.evaluation.users.models.entity.Phone;
import com.evaluation.users.models.entity.User;
import com.evaluation.users.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServices implements IUserServices {
    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    public IUserDao UserDao;

    @Override
    public List<User> listAll() {
        List<User> result = new ArrayList<>();
        UserDao.findAll().forEach(i->result.add(i));
        return result;
    }

    @Override
    public User save(User item) {
        item.setToken(jwtUtil.generateToken(item));
        User user = null;
        user = findByEmail(item.getEmail());
        if (user != null){
            throw new Exception("El correo ya existe");
        }
        return UserDao.save(item);
    }

    @Override
    public User findById(Long id) {
        return null;
    }

    @Override
    public User findByEmail(String email) {
        return UserDao.getPhonesByUserEmail(email);
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<Phone> getPhonesByUserId(Long id) {
        return UserDao.getPhonesByUserId(id);
    }
}
