package com.evaluation.users.models.services;

import com.evaluation.users.models.dao.IPhoneDao;
import com.evaluation.users.models.entity.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhoneServices implements IPhoneServices {
    @Autowired
    public IPhoneDao PhoneDao;

    @Override
    public List<Phone> listAll() {
        List<Phone> result = new ArrayList<>();
        PhoneDao.findAll().forEach(item -> {
            result.add(item);
        });
        return result;
    }

    @Override
    public Phone save(Phone item) {
        return PhoneDao.save(item);
    }

    @Override
    public Phone findById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
