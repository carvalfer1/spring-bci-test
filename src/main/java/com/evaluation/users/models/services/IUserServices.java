package com.evaluation.users.models.services;

import com.evaluation.users.models.entity.Phone;
import com.evaluation.users.models.entity.User;

import java.util.List;

public interface IUserServices {
    List<User> listAll();
    User save(User item);
    User findById(Long id);
    User findByEmail(String email);
    void delete(Long id);
    List<Phone> getPhonesByUserId(Long id);
}
