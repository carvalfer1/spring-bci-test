package com.evaluation.users.models.services;

import com.evaluation.users.models.entity.Phone;

import java.util.List;

public interface IPhoneServices {
    List<Phone> listAll();
    Phone save(Phone item);
    Phone findById(Long id);
    void delete(Long id);
}
