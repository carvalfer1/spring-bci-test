package com.evaluation.users.models.dao;
import com.evaluation.users.models.entity.Phone;
import com.evaluation.users.models.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IUserDao extends CrudRepository<User, Long> {
    @Query("select v FROM Phone v where user_id = ?1")
    public List<Phone> getPhonesByUserId(Long id);

    @Query("select v FROM User v where email = ?1")
    public User getPhonesByUserEmail(String email);

}
