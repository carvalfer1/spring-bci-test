package com.evaluation.users.models.dao;
import com.evaluation.users.models.entity.Phone;
import org.springframework.data.repository.CrudRepository;

public interface IPhoneDao extends CrudRepository<Phone, Long> {
}
