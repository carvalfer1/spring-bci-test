package com.evaluation.users.models.maps;

import com.evaluation.users.models.entity.User;
import com.evaluation.users.models.maps.targets.UserResponse;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface IUserResponseMapper {
    @Mapping(source = "lastLogin", target = "last_login")
    @Mapping(source = "active", target = "isactive")
    UserResponse userToUserResponse(User user);
}
