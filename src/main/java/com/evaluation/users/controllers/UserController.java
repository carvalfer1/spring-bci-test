package com.evaluation.users.controllers;

import com.evaluation.users.models.entity.User;
import com.evaluation.users.models.maps.IUserResponseMapper;
import com.evaluation.users.models.maps.targets.UserResponse;
import com.evaluation.users.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import com.evaluation.users.models.services.IUserServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    IUserServices UserServices;

    @Autowired
    IUserResponseMapper userResponseMapper;

    @GetMapping
    public List<User> List(){
        return UserServices.listAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) //201
    public UserResponse Save(@RequestBody User newUser){
        return userResponseMapper.userToUserResponse(UserServices.save(newUser));
    }

}
