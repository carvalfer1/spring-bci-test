package com.evaluation.users.controllers;
import com.evaluation.users.models.entity.Phone;
import com.evaluation.users.models.services.IUserServices;
import com.evaluation.users.models.services.IPhoneServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/phone")
public class PhoneController {
    @Autowired
    IPhoneServices PhoneServices;
    @Autowired
    IUserServices UserServices;

    @GetMapping
    public List<Phone> List(){
        return PhoneServices.listAll();
    }

    @PostMapping
    public Phone Save(@RequestBody Phone newPhone){
        return PhoneServices.save(newPhone);
    }

    @GetMapping("/user/{id}")
    public List<Phone> getPhones(@PathVariable Long id){
        return UserServices.getPhonesByUserId(id);
    }
}
