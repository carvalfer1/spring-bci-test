package com.evaluation.users.security;

import com.evaluation.users.models.entity.User;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Service;


import java.security.Key;
import java.util.Date;

@Service
public class JwtUtil {
    @Value("${jwt.secret}")
    private String secret;

    String  getSecret() {
        int len = 512 / 8;
        String secretUsed = secret != null ? secret : "";

        if (secretUsed.length() < len ) {
            secretUsed = StringUtils.rightPad(secretUsed,len,"0");
        }
        return secretUsed;
    }

    public String generateToken(User u) {
        Key key = Keys.hmacShaKeyFor(getSecret().getBytes());
        String jws = Jwts.builder().setSubject(u.getName()).signWith(key, SignatureAlgorithm.HS512).compact();
        return jws;
    }
}
